<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 4/12/2023
  Time: 4:44 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <style>
        th, td{
            border: 2px solid black  ;
        }
    </style>
</head>
<body>
<table>
    <tr>
        <td>id</td>
        <td>card number</td>
        <td>balance</td>
        <td>code</td>
        <td>edit</td>
        <td>delete</td>
    </tr>

    <c:forEach var="card" items="${cardList}">
        <tr>
            <th>${card.id}</th>
            <th>${card.cardNumber}</th>
            <th>${card.balance}</th>
            <th>${card.code}</th>
            <th><a href="/api/card/edit/${card.id}"><button>Edit</button></a></th>
        </tr>
    </c:forEach>
</table>
<br>
<h1>Add another<a href="/api/card/add">card</a></h1>
</body>
</html>
