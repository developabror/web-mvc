package uz.topskill.controller;

import org.eclipse.tags.shaded.org.apache.xpath.operations.Mod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import uz.topskill.entity.Card;
import uz.topskill.repository.CardRepository;

import java.util.List;

@Controller
@RequestMapping("/api/card")
public class CardController {
    @Autowired
    private CardRepository cardRepository;

    @RequestMapping(path = "/add",method = RequestMethod.GET)
    public String getAddPage(Model model){
        Card myCard = new Card();
        model.addAttribute("card",myCard);
        return "addCard";
    }

    @RequestMapping(value = "/add",method = RequestMethod.POST)
    public String addCard(@ModelAttribute Card card){
        cardRepository.addCard(card);
        return "redirect:/api/card/show";
    }

    @RequestMapping("/show")
    public String getAllCards(Model model){
        List<Card> cards = cardRepository.getCards();
        model.addAttribute("cardList",cards);
        return "cards";
    }

    @RequestMapping(path = "/edit/{cardId}",method = RequestMethod.GET)
    public String editCardPage(@PathVariable Integer cardId,Model model){
        Card card = cardRepository.getCard(cardId);
        model.addAttribute("card",card);
        return "editCard";
    }
    @RequestMapping(path = "/edit",method = RequestMethod.POST)
    public String editCard(@ModelAttribute Card card){
        System.out.println(card);
        return "editCard";
    }
}
