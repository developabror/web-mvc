package uz.topskill.repository;

import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import uz.topskill.entity.Card;

import java.sql.ResultSet;
import java.util.List;

@Repository
public class CardRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;



    public void addCard(Card card){
        jdbcTemplate.update(
                "insert into card(card_number,code,balance) values (?,?,?)",
                card.getCardNumber(),
                card.getCode(),
                card.getBalance()
        );
    }
    public List<Card> getCards(){
        return jdbcTemplate.query("select * from card",
                (rs, rowNum) -> getCardFromResultSet(rs));
    }

    public Card getCard(Integer id){
        return jdbcTemplate.queryForObject("select * from card where id = ?",
                new Object[]{id},
                (rs, rowNum) -> getCardFromResultSet(rs));
    }

    @SneakyThrows
    private Card getCardFromResultSet(ResultSet rs) {
        return new Card(
                rs.getInt("id"),
                rs.getString("card_number"),
                rs.getString("code"),
                rs.getDouble("balance")

        );
    }
}
