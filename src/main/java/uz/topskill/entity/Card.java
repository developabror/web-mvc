package uz.topskill.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Card {
    private Integer id;
    private String cardNumber;
    private String code;
    private Double balance;
}
