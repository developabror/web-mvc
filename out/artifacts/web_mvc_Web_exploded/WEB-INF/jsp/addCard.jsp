<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 4/12/2023
  Time: 4:33 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>

<form:form
        modelAttribute="card"
        action="/api/card/add"
        method="post">

    <table>
        <tr>
            <th>card number: </th>
            <th><form:input path="cardNumber"/></th>
        </tr>
        <tr>
            <th>card code: </th>
            <th><form:input path="code"/></th>
        </tr>
        <tr>
            <th>card balance: </th>
            <th><form:input path="balance"/></th>
        </tr>
        <tr>
            <form:button> Submit: </form:button>
        </tr>
    </table>
</form:form>

</body>
</html>
